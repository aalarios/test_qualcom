<?php

namespace App\Models;

use App\Scopes\RoomsScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rooms extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'min_adults',
        'max_adults',
        'min_children',
        'max_children',
        'total'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new RoomsScope);
    }
}
