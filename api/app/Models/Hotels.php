<?php

namespace App\Models;

use App\Scopes\HotelsScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotels extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'address',
        'city',
        'phone',
        'total_rooms',
        'active',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new HotelsScope);
    }

    public function scopeStockRooms( $query, $h, $rooms ){
        return $query->whereHas( 'rooms', function( $query ) use ( $h, $rooms ){
            foreach( $h as $id )
                $query->whereIn( 'rooms.id', $rooms[$id] );
                
        });

    }

    public function rooms(){
        return $this->hasMany( \App\Models\Rooms::class, 'hotel_id' );
    }
}
