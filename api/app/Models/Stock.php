<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;

    protected $table = 'stock';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotel_id',
        'room_id',
        'initial',
        'ended',
        'amount_by_adult',
        'amount_by_child'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function hotels(){
        return $this->hasOne( \App\Models\Hotels::class, 'id' );
    }

    public function rooms(){
        return $this->hasOne( \App\Models\Rooms::class, 'id' );
    }
}
