<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use App\Models\Rooms;
use App\Models\Hotels;
use App\Models\Reservations;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StockController extends Controller
{
    public function search( Request $request ){
        $newOrder = [];
        $validator = Validator::make( $request->all(), [
            'city' => 'bail|required',
            'check_in' => 'required|date|after:now',
            'check_out' => 'required|date|after:check_in',
            'adults' => 'required|min:1,max:7',
            'children' => 'bail|require|min:1,max:4'
        ]);
        if ($validator->fails())
            return response([
                'code' => 400,
                'status' => false,
                'message' => 'Something was wrong, try againg.',
                'data' => []
            ], 400 );
        $newOrder = [
            'identity' => ( string )Str::uuid(),
            'adults' => ( int ) $request->adults,
            'children' => ( int ) $request->children ?? 0,
            'check_in' => \Carbon\Carbon::create( $request->check_in )->toDateTimeString(),
            'check_out' => \Carbon\Carbon::create( $request->check_out )->toDateTimeString(),
            'nights' => \Carbon\Carbon::create( $request->check_out )->diffInDays( \Carbon\Carbon::create( $request->check_in ) )
        ];
        $res = Stock::where( 'initial', '>=', $request->check_in )->get();
        $newRes = [];
        $hotels = [];
        $rooms = [];
        $prices = [];
        foreach( $res as $key => $val ){
            $hotel_id = $val->hotel_id;
            $room_id = $val->room_id;
            unset( $val->room_id );
            unset( $val->hotel_id );
            unset( $val->initial );
            unset( $val->ended );
            $hotels[] = $hotel_id;
            $rooms[ $hotel_id ][] = $room_id;
            $prices[ $room_id ][] = $val;
        }
        $h = 0;
        $res = Hotels::with( 'rooms' )->whereIn( 'id', $h );
        if( isset( $request->city ) )
            $res->where( 'city', $request->city );
        $res->get();
        $data = [];
        if( !$res->isEmpty() ){
            $h = $res->count();
            foreach( $res as $key => $val ){
                $val[ 'image' ] = url( '/uploads/hotels/' . $val->id . '/logo.png' );
                $newRooms = [];
                foreach( $val->rooms as $r ){
                    if( in_array( $r->id, $rooms[ $val->id ] ) ){
                        if( $request->adults <= $r->max_adults ){
                            if( isset( $request->children ) && $request->children <= $r->max_children ){
                                $r[ 'images' ] = [
                                    url( '/uploads/hotels/' . $val->id . '/rooms/' . Str::lower( str_replace( " ", "_", $r->name ) ) . '/1.png' ),
                                    url( '/uploads/hotels/' . $val->id . '/rooms/' . Str::lower( str_replace( " ", "_", $r->name ) ) . '/2.png' ),
                                ];
                                $r[ 'prices' ] = $prices[ $r->id ][0];
                                $newRooms[] = $r;
                            } else {
                                $r[ 'images' ] = [
                                    url( '/uploads/hotels/' . $val->id . '/rooms/' . Str::lower( str_replace( " ", "_", $r->name ) ) . '/1.png' ),
                                    url( '/uploads/hotels/' . $val->id . '/rooms/' . Str::lower( str_replace( " ", "_", $r->name ) ) . '/2.png' ),
                                ];
                                $r[ 'prices' ] = $prices[ $r->id ][0];
                                $newRooms[] = $r;
                            }
                        }
                    }
                }
                unset( $val[ 'rooms' ] );
                $val[ 'rooms' ] = $newRooms;
                $data[] = $val;
            }
        }
        \Cache::put( 'preOrder', $newOrder );
        return response([
            'code' => 200,
            'message' => 'Ok',
            'status' => true,
            'info' => [
                'totalRecords' => $h,
                'data' => $data,
            ]
        ], 200 );
    }

    public function preview( Request $request ) {
        $validator = Validator::make( $request->all(), [
            'fullname' => 'required',
            'email' => 'required|email',
            'stock_id' => 'required|numeric|min:1'
        ]);
        if ($validator->fails())
            return response([
                'code' => 400,
                'status' => false,
                'message' => 'Something was wrong, try againg.',
                'data' => []
            ], 400 );
        $preOrder = \Cache::get( 'preOrder' );
        $preOrder[ 'infoBuyer' ] = [
            'name' => $request->fullname,
            'email' => $request->email,
            'payment_method' => 'credit-card',
        ];
        try{
            $stock = Stock::findOrFail( $request->stock_id );
            $hotel = [];
            foreach( Hotels::where( 'id', $stock->hotel_id )->get([ 'id', 'name', 'email', 'description', 'address', 'phone', 'city' ])->toArray() as $val ){
                $val[ 'image' ] = url( '/uploads/hotels/' . $stock->hotel_id . '/logo.png' );
                $hotel = $val;
            }
            $room = [];
            foreach( Rooms::where( 'id', $stock->room_id )->get([ 'id', 'name', 'type', 'description' ])->toArray() as $val ){
                $val[ 'image' ] = [
                    url( '/uploads/hotels/' . $stock->hotel_id . '/rooms/' . Str::lower( str_replace( " ", "_", $val[ 'name' ] ) ) . '/1.png' ),
                    url( '/uploads/hotels/' . $stock->hotel_id . '/rooms/' . Str::lower( str_replace( " ", "_", $val[ 'name' ] ) ) . '/2.png' ),
                ];
                $room = $val;
            }
            $preOrder[ 'selection' ] = [
                'hotel' => $hotel,
                'room' => $room
            ];

            $priceAdult = ( int )$preOrder[ 'adults' ] * ( int )$stock->amount_by_adult * ( int )$preOrder[ 'nights' ];
            $priceChildren = ( int )$preOrder[ 'children' ] * ( int )$stock->amount_by_child * ( int )$preOrder[ 'nights' ];
            
            $preOrder[ 'prices' ] = [
                'sub_total' => $priceAdult + $priceChildren,
                'taxes' => 1.16
            ];
            $preOrder[ 'total' ] = ( int )$preOrder[ 'prices' ][ 'sub_total' ] * ( float )$preOrder[ 'prices' ][ 'taxes' ];
            \Cache::put( 'preOrder', $preOrder );
            return response([
                'code' => 201,
                'status' => true,
                'message' => 'Preview',
                'data' => $preOrder
            ]);
        } catch( \Illuminate\Database\Eloquent\ModelNotFoundException $e ){
            return response([
                'code' => 404,
                'status' => false,
                'message' => 'Room not found',
                'data' => []
            ]);
        }
        
    }

    public function reservation( Request $request ){
        $validator = Validator::make( $request->all(), [
            'accept' => 'required',
        ]);
        if ($validator->fails())
            return response([
                'code' => 409,
                'status' => false,
                'message' => 'You must accept to generate the payment, try againg.',
                'data' => []
            ], 409 );
        if( $request->accept == false ){
            \Cache::flush();
            return response([
                'code' => 406,
                'status' => false,
                'message' => 'Your payment has beed cancelled',
                'data' => []
            ], 406 );
        }
        if( !\Cache::has( 'preOrder' ) && $request->accept == true )
            return response([
                'code' => 406,
                'status' => false,
                'message' => 'Your payment has beed cancelled',
                'data' => []
            ], 406 );
        \DB::beginTransaction();
        try {
            $newOrder = \Cache::get( 'preOrder' );
            $reservation = new Reservations();
            $reservation->identity = $newOrder[ 'identity' ];
            $reservation->hotel_id = $newOrder[ 'selection' ][ 'hotel' ][ 'id' ];
            $reservation->details = json_encode( \Cache::get( 'preOrder' ) );
            $reservation->save();
            // $this->sendEmail( \Cache::get( 'preOrder' ) );
            \Cache::flush();
            \DB::commit();
            return response([
                'code' => 201,
                'status' => true,
                'message' => 'Your reservation has beed completed, You will receive an email with the details of your reservation.',
                'data' => []
            ], 201 );
        } catch( \Exception $e ){
            \DB::rollBack();
            return response([
                'code' => 409,
                'status' => true,
                'message' => $e->getMessage(),
                'data' => []
            ], 409 );
        }
    }
}
