<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Stock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->id();
            $table->integer('hotel_id')->unsigned()->index('s_hotel_id');
            $table->integer('room_id')->unsigned()->index('s_room_id');
            $table->date('initial');
            $table->date('ended');
            $table->integer('amount_by_adult')->unsigned()->index('room_amount_by_adult')->default(0);
            $table->integer('amount_by_child')->unsigned()->index('room_amount_by_child')->default(0);
            // $table->integer('discount_by_adult')->unsigned()->index('room_discount')->default(0);
            // $table->integer('discount_by_children')->unsigned()->index('room_discount')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
