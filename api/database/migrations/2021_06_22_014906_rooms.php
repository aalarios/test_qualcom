<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index('room_name');
            $table->string('slug')->unique();
            $table->string('type')->index('type_room');
            $table->integer('min_adults')->unsigned()->index('min_adults_room')->default(1);
            $table->integer('max_adults')->unsigned()->index('max_adults_room')->default(0);
            $table->integer('min_children')->unsigned()->index('min_children_room')->default(0);
            $table->integer('max_children')->unsigned()->index('max_children_room')->default(0);
            $table->integer('total')->unsigned()->index('total_room');
            $table->boolean('active');
            $table->longText('description');
            $table->integer('hotel_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
