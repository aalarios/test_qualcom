<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->loadHotels();
        $this->loadRooms();
        $this->loadStock();
    }

    private function loadHotels(){
        $names = [
            'The Decameron',
            'Mayan Palace',
            'The Crown Paradise',
            'Gran Festival',
            'Palladium Travel Club'
        ];

        $cities = [
            'Bucerias',
            'Nuevo Vallarta',
            'Puerto Vallarta',
            'Manzanillo',
            'Punta Mita'
        ];

        $email = [
            'info@decameron.com',
            'info@vidantagroup.com',
            'info@arrivahotels.com',
            'info@granfestivall.com',
            'info@thepalladiumgroup.com'
        ];

        $description = [
            'At Decameron Hotels it has always been our priority to protect the health, safety and well-being of all our guests, affiliates and employees. Under the current circumstances, we have strengthened our protocols and implemented practices and processes of the highest level that will be continuously verified by our operations team.',
            "Welcome to luxury vacationing done right. Welcome to the world of Vidanta, a collection of lavish resorts and various curated vacation experiences located throughout Mexico's most beautiful landscapes. Boasting high-end amenities, comfort-filled accommodations, and the best service in Mexico, there's nothing quite like a vacation with Vidanta",
            'In Arriva Hospitality Group we have operated hotels for more than 50 years in different destinations throughout Mexico. We offer trust and excellent profit margins to our investors and unique experiences to our guests, whether its a vacation, business trip or a congress, we achieve a loyalty that is renewed year after year.',
            'Beautiful hotel dressed in white color with mediterranean style developed in 32 acres full of tropical nature, located in the hotelier zone of Manzanillo, Colima; in front of the warm Miramar beach in the Mexican Pacific Ocean.',
            'Palladium Hotel GroupPertaining to Grupo Empresas Matutes, we are an internationally renowned Spanish hotel chain that first emerged towards the end of the 1960´s. Currently present in 6 countries, we are made up of 50 different hotels located in Spain, Mexico, the Dominican Republic, Jamaica, Sicily (Italy), and Brazil.'
        ];

        $address = [
            'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7462.1849802018205!2d-105.327836!3d20.747045000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfb0ffa6a05891191!2sRoyal+Decameron+Complex!5e0!3m2!1ses!2sus!4v1507905745849',
            'https://www.google.com.mx/maps/place/Mayan+Palace+at+Vidanta+Nuevo+Vallarta/@20.6802809,-105.2855795,17z/data=!4m8!3m7!1s0x84214423986ff1d5:0x8fa9eb1d320bc032!5m2!4m1!1i2!8m2!3d20.6809028!4d-105.2859169',
            'https://www.google.com.mx/maps/place/Crown+Paradise+Golden+Resort+Puerto+Vallarta/@20.6510431,-105.2452211,17z/data=!4m12!1m2!2m1!1sthe+crown+paradise!3m8!1s0x842a98b27e0503e3:0xcba6d311bb7bfbb0!5m2!4m1!1i2!8m2!3d20.6506323!4d-105.2427146!15sChJ0aGUgY3Jvd24gcGFyYWRpc2VaFCISdGhlIGNyb3duIHBhcmFkaXNlkgEFaG90ZWyaASNDaFpEU1VoTk1HOW5TMFZKUTBGblNVTnZObHAyYTBWUkVBRQ',
            'https://www.google.com/maps/place/Gran+Festivall+All+Inclusive+Resort/@19.1216207,-104.3780346,17.26z/data=!4m12!1m2!2m1!1sgran+festivall!3m8!1s0x8424d76d10fdd09b:0xc650f53b213ceaea!5m2!4m1!1i2!8m2!3d19.1233506!4d-104.3760823!15sCg5ncmFuIGZlc3RpdmFsbFoQIg5ncmFuIGZlc3RpdmFsbJIBBWhvdGVs',
            'https://www.google.com/maps?q=palladium+travel+club&um=1&ie=UTF-8&sa=X&ved=2ahUKEwjg66SYwK7xAhUOlKwKHentDGgQ_AUoA3oECAEQBQ'
        ];

        $phone = [
            '800-011-11-11',
            '1-800-292-9446',
            '800-900-09-00',
            '31-43-35-21-50',
            '329-226-99-00'
        ];

        for( $x = 1; $x <= 5; $x++ ){
            \App\Models\Hotels::create([
                'name' => $names[ $x-1 ],
                'slug' => Str::lower( str_replace( " ", "_", $names[ $x-1 ] ) ),
                'email' => $email[ $x-1 ],
                'city' => $cities[ $x-1 ],
                'address' => $address[ $x-1 ],
                'phone' => $phone[ $x-1 ],
                'total_rooms' => 6,
                'active' => true,
                'description' => $description[ $x-1 ],
            ]);
        }
    }

    private function loadRooms(){
        $names = [
            'Suite Garden View',
            'Junior Suite Sea View',
            'Junior Suite Premium Level',
            'Master Suite Sea Front View Premium Level',
            'Master Suite Premium Level with Sea Front View and Private Swimming Pool',
            'Presidential Suite Ocean Front Premium Level',
        ];

        $description = [
            'These 732 ft² suites have a private terrace or balcony with partial views of the pool and gardens. These comfortable, bright rooms are decorated in a contemporary style and located from the first to the seventh floor. They are the best way to enjoy a family vacation without missing out on the exclusivity of the most distinguished services. PLEASE NOTE: For rooms with a king-size bed or two double beds, the type of bed is subject to availability.',
            'These large and comfortable rooms of 68 m² with partial views of the sea from balcony. Located between the second and seventh floors, these rooms are fully equipped to satisfy the most discerning requirements. They stand out for their meticulous design and luminosity, in harmony with the hotel’s architecture – inspired by Mayan Temples. PLEASE NOTE: the bed type is subject to availability.',
            'These elegant 68 m2 rooms have a balcony with sea views and all the amenities necessary to satisfy the most discerning tastes. Located on the sixth and seventh floors of the hotel, they boast some truly privileged views. It includes all of the Premium Level services to round out a perfect family vacation. PLEASE NOTE: the bed type is subject to availability.',
            'These marvellous suites 117 m2 have stunning sea views, undoubtedly the best backdrop to enjoy a pleasant stay. They have 2 bedrooms that are ideal for family vacations with a little intimacy. The spacious, comfortable and bright suites are strategically located on the fifth, sixth and seventh floors of the hotel. Premium Level services designed to satisfy all guest needs are also included. PLEASE NOTE: Room service does not include alcoholic beverages.',
            'This spectacular 329 m2 suite has two bedrooms, making it ideal for families or a romantic holiday. From its exclusive location between the second and seventh floors of the hotel, you’ll enjoy incredible views of Bahía de Manzanillo. It’s the most exclusive option for enjoying a pleasant few days of genuine relaxation. All of the Premium Level services are included and they are unquestionably the best way to get the most out of the luxurious facilities at this magnificent 5-star hotel.',
            'This incredible suite is the most exclusive in the hotel, it has 337 m² and located on the seventh floor, where you can enjoy the best views of the bay. It is divided into 3 separate rooms and fully equipped to provide greater comfort, everything required to satisfy the most discerning tastes and to enjoy a luxurious, relaxed vacation. Premium Level services are included, highlighting the exclusivity of a 5-star hotel and attending to every detail to satisfy all guest wishes. PLEASE NOTE: Room service does not include alcoholic beverages.'
        ];

        foreach( \App\Models\Hotels::get() as $key => $val )
            for( $x = 0; $x < 6; $x++ )
                \App\Models\Rooms::create([
                    'name' => $names[ $x ],
                    'slug' => $val->id.'_'.Str::lower( str_replace( " ", "_", $names[ $x ] ) ),
                    'type' => 'All Inclusive',
                    'min_adults' => ($x >= 3 ) ? 2 : 1,
                    'max_adults' => ($x >= 3 ) ? 7 : 3,
                    'min_children' => 0,
                    'max_children' => 2,
                    'total' => 1,
                    'active' => true,
                    'hotel_id' => $val->id,
                    'description' => $description[ $x ],
                ]);
    }

    private function loadStock(){
        $amounts_adults = [
            0,
            130,
            142,
            158,
            219,
            289,
            343
        ];
        $amounts_children = [
            0,
            39,
            59,
            89,
            110,
            139,
            199
        ];
        $dates_available = [
            0,
            [
                [ 1, 'day' ],
                [ 1, 'year' ],
            ], [
                [ 5, 'day' ],
                [ 5, 'month' ],
            ], [
                [ 1, 'day' ],
                [ 2, 'month' ],
            ], [
                [ 15, 'day' ],
                [ 1, 'month' ],
            ], [
                [ 20, 'day' ],
                [ 7, 'month' ],
            ], [
                [ 1, 'day' ],
                [ 15, 'day' ],
            ]
        ];
        $hotel = "";
        $number = 1;
        foreach( \App\Models\Rooms::get() as $key => $val ){
            $number = ( $hotel === $val->hotel_id ) ? ++$number : 1;
            \App\Models\Stock::create([
                'hotel_id' => $val->hotel_id,
                'room_id' => $val->id,
                'initial' => \Carbon\Carbon::now()->add( $dates_available[$number][0][0], $dates_available[$number][0][1] ),
                'ended' => \Carbon\Carbon::now()->add( $dates_available[$number][1][0], $dates_available[$number][1][1] ),
                'amount_by_adult' => $amounts_adults[ $number ],
                'amount_by_child' => $amounts_children[ $number ],
            ]);
            $hotel =+ $val->hotel_id;
        }
    }
}
