ara poder hacer funcionar el sitio en desarrollo, es necesario tener instalado los siguientes programas:
  - [Docker](https://www.docker.com/get-started)
  - [Docker Compose](https://docs.docker.com/compose/install/)

# Instrucciones!
  - Seguir los siguientes pasos:
```git
$ git remote add origin git@gitlab.com:aalarios/test_qualcom.git
$ git pull origin main
$ docker-compose up -d --build
```
> Para revisar si los contenedores estan corriendo correctamente ingresar el siguiente comando:
```php
$ docker ps
```
> Cuando se haya creado el proyecto y esten corriendo los 3 contenedores ingresar la siguiente instruccion:
```php
$ docker exec api composer install
$ docker exec api php artisan migrate --seed
```
> y listo. Para ver que todo esta jalando correctamente ingresar a [localhost]http://localhost

License
----

MIT